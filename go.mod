module echoserver

go 1.13

require (
	github.com/0xAX/notificator v0.0.0-20191016112426-3962a5ea8da1 // indirect
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496 // indirect
	github.com/codegangsta/envy v0.0.0-20141216192214-4b78388c8ce4 // indirect
	github.com/codegangsta/gin v0.0.0-20171026143024-cafe2ce98974 // indirect
	github.com/cosiner/argv v0.0.1 // indirect
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-delve/delve v1.3.2 // indirect
	github.com/go-openapi/analysis v0.19.7 // indirect
	github.com/go-openapi/errors v0.19.3 // indirect
	github.com/go-openapi/runtime v0.19.10 // indirect
	github.com/go-openapi/spec v0.19.5 // indirect
	github.com/go-openapi/strfmt v0.19.4 // indirect
	github.com/go-openapi/swag v0.19.7 // indirect
	github.com/go-openapi/validate v0.19.6 // indirect
	github.com/go-ozzo/ozzo-validation/v3 v3.8.1
	github.com/go-swagger/go-swagger v0.21.0 // indirect
	github.com/gorilla/websocket v1.4.1
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/mattn/go-runewidth v0.0.7 // indirect
	github.com/mattn/go-shellwords v1.0.6 // indirect
	github.com/mikefarah/yq/v2 v2.4.1 // indirect
	github.com/pdrum/swagger-automation v0.0.0-20190629163613-c8c7c80ba858
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/peterh/liner v1.1.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.6.2
	github.com/valyala/fasttemplate v1.1.0 // indirect
	github.com/wzshiming/gen v0.2.11 // indirect
	go.mongodb.org/mongo-driver v1.2.1 // indirect
	go.starlark.net v0.0.0-20191227232015-caa3e9aa5008 // indirect
	golang.org/x/arch v0.0.0-20191126211547-368ea8f32fff // indirect
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876 // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	golang.org/x/sys v0.0.0-20200124204421-9fbb57f87de9 // indirect
	golang.org/x/tools v0.0.0-20200125223703-d33eef8e6825 // indirect
	gopkg.in/ini.v1 v1.51.1 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/urfave/cli.v1 v1.20.0 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
