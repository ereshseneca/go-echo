package handlers

import (
	"echoserver/utils"
	"echoserver/vo"
	"encoding/json"
	"fmt"
	"github.com/labstack/echo"
	log "github.com/sirupsen/logrus"
	"net/http"
)

//http://localhost:8000/cats/json?name=arnold&type=fluffyapi.
// #route:"GET /"#
func GetCats(c echo.Context) error {
	catName := c.QueryParam("name")
	catType := c.QueryParam("type")
	msg := c.QueryParam("msg")
	dataType := c.Param("data")
	log.Info("Messages is --", msg)
	log.WithFields(log.Fields{
		"catName": catName,
		"catType": catType,
	}).Info("Request for Get Cats")
	if dataType == "string" {
		return c.String(http.StatusOK, fmt.Sprintf("your cat name is : %s\nand cat type is : %s\n", catName, catType))
	} else if dataType == "json" {
		cat := vo.Cat{
			Name: catName,
			Type: catType,
		}

		log.WithFields(log.Fields{
			"data": &cat,
		}).Info("Response for Get cats")
		response := utils.Message(true, "Account has been created", http.StatusOK)
		response["cat"] = cat
		//return c.JSON(http.SatusOK, cat)
		return c.JSON(http.StatusOK, cat)
	} else {
		return c.JSON(http.StatusBadRequest, map[string]string{
			"error": "Please specify the data type as Sting or JSON",
		})
	}

}

func AddCat(c echo.Context) error {
	cat := vo.Cat{}
	defer c.Request().Body.Close()

	err := json.NewDecoder(c.Request().Body).Decode(&cat)
	if err == nil {
		log.Fatalf("Failed reading the request body %s", err)
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error)
	}
	log.Printf("this is yout cat %#v", cat)
	return c.String(http.StatusOK, "We got your Cat!!!")
}
